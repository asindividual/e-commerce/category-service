package name.alp.shopping.categoryservice;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;

@Repository
public interface CategoryRepository extends ReactiveCrudRepository<Category ,String> {
    @Query("{'parent.id': ?0 ")
    Flux<Category> findByParentId(String categoryId);
}
