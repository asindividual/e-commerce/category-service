package name.alp.shopping.categoryservice;

import io.swagger.v3.oas.annotations.Operation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class CategoryRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CategoryRestController.class);
    private final CategoryService categoryService;

    public CategoryRestController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @Operation(summary = "Provides save function for the argument",
            description = "This endpoint creates a new record if the argument doesnt have an id, if the argument has a proper id than it updates" )
    @PostMapping
    public Mono<Category> save(@RequestBody Category category) {
        LOGGER.info("create: {}", category);
        return categoryService.save(category);
    }

    @Operation(summary = "Provides save function for the argument",
            description = "This endpoint creates a new record if the argument doesnt have an id, if the argument has a proper id than it updates" )
    @GetMapping("/{id}")
    public Mono<Category> findById(@PathVariable("id") String id) {
        LOGGER.info("findById: id={}", id);
        return categoryService.findById(id);
    }
    @Operation(summary = "Provides save function for the argument",
            description = "This endpoint creates a new record if the argument doesnt have an id, if the argument has a proper id than it updates" )
    @GetMapping
    public Flux<Category> findAll() {
        LOGGER.info("findAll");
        return categoryService.findAll();
    }



    @GetMapping("/parentCategory/{category}")
    public Flux<Category> findByParentId(@PathVariable("category") String categoryId) {
        LOGGER.info("findByParentId: category={}", categoryId);
        return categoryService.findByParentId(categoryId);
    }



}
