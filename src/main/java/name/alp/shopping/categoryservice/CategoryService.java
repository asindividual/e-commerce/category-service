package name.alp.shopping.categoryservice;

import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class CategoryService {

    private final CategoryRepository categoryRepository;

    public CategoryService(CategoryRepository categoryRepository) {
        this.categoryRepository = categoryRepository;
    }

    public Mono<Category> save(Category category) {
        return categoryRepository.save(category);
    }

    public Mono<Category> findById(String id) {
        return categoryRepository.findById(id);
    }


    public Flux<Category> findAll() {
        return categoryRepository.findAll();
    }

    public Flux<Category> findByParentId(String categoryId) {
        return categoryRepository.findByParentId(categoryId);
    }
}
