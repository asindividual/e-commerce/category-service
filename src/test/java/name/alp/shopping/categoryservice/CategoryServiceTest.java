package name.alp.shopping.categoryservice;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.assertj.core.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@WebFluxTest(controllers = CategoryRestController.class)
@Import(CategoryService.class)
public class CategoryServiceTest {

    @Autowired
    private  CategoryService categoryService;


    @MockBean
    CategoryRepository repository;

    @Test
    public void saveTest(){
        Category category = new Category();
        category.setId("test_id1");
        category.setTitle("Test title");
        Mockito.when(repository.save(category)).thenReturn(Mono.just(category));

        Mono<Category> result =  categoryService.save(category);

        assertThat(result).isNotNull();
    }

    @Test
    public void findById(){
        Category category = new Category();
        category.setId("test_id1");
        category.setTitle("Test title");

        Mockito
                .when(repository.findById("test_id1"))
                .thenReturn(Mono.just(category));

        Mono<Category> result =  categoryService.findById("test_id1");
        assertThat(result).isNotNull();
    }

    @Test
    public void findAllTest(){
        Category category1 = new Category();
        category1.setId("test_id1");
        category1.setTitle("Test title");
        Category category2 = new Category();
        category2.setId("test_id2");
        category2.setTitle("Test title");
        Mockito
                .when(repository.findAll())
                .thenReturn(Flux.fromArray(new Category[]{category1,category2}));

        Flux<Category> result =  categoryService.findAll();
        assertThat(result.collectList().block().size()).isEqualTo(2);
    }

    @Test
    public void findByParentIdTest(){
        Category category1 = new Category();
        category1.setId("test_id1");
        category1.setTitle("Test title");
        Category category2 = new Category();
        category2.setId("test_id2");
        category2.setTitle("Test title");
        category2.setParent(category1);
        Category category3 = new Category();
        category3.setId("test_id3");
        category3.setTitle("Test title");
        category3.setParent(category1);

        Mockito
                .when(repository.findByParentId("test_id1"))
                .thenReturn(Flux.fromArray(new Category[]{category3,category2}));

        Flux<Category> result =  categoryService.findByParentId("test_id1");
        assertThat(result.collectList().block().size()).isEqualTo(2);
    }
}
