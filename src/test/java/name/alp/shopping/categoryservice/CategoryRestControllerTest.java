package name.alp.shopping.categoryservice;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.EntityExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


import static org.mockito.Mockito.times;


@ExtendWith(SpringExtension.class)
@WebFluxTest(controllers = CategoryRestController.class)
@Import(CategoryService.class)
public class CategoryRestControllerTest {

    @MockBean
    CategoryRepository repository;

    @Autowired
    private WebTestClient webClient;

    @Test
    void testSave() {
        Category category = new Category();
        category.setId("test_id1");
        category.setTitle("Test title");


        Mockito.when(repository.save(category)).thenReturn(Mono.just(category));

        webClient.post()
                .uri("/")
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromObject(category))
                .exchange()
                .expectStatus().isOk();

    }

    @Test
    void testFindById()
    {
        Category category = new Category();
        category.setId("test_id1");
        category.setTitle("Test title");

        Mockito
                .when(repository.findById("test_id1"))
                .thenReturn(Mono.just(category));

        webClient.get().uri("/{id}", "test_id1")
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.title").isNotEmpty()
                .jsonPath("$.id").isEqualTo("test_id1");

        Mockito.verify(repository, times(1)).findById("test_id1");
    }

    @Test
    void testFindAll()
    {
        Category category1 = new Category();
        category1.setId("test_id1");
        category1.setTitle("Test title");
        Category category2 = new Category();
        category2.setId("test_id2");
        category2.setTitle("Test title");
        category2.setParent(category1);
        Category category3 = new Category();
        category3.setId("test_id3");
        category3.setTitle("Test title");
        category3.setParent(category1);

        Mockito
                .when(repository.findAll())
                .thenReturn(Flux.fromArray(new Category[]{category1,category2,category3}));

        webClient.get().uri("/")
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(Category.class);


    }

    @Test
    public void findByParentIdTest(){
        Category category1 = new Category();
        category1.setId("test_id1");
        category1.setTitle("Test title");
        Category category2 = new Category();
        category2.setId("test_id2");
        category2.setTitle("Test title");
        category2.setParent(category1);
        Category category3 = new Category();
        category3.setId("test_id3");
        category3.setTitle("Test title");
        category3.setParent(category1);

        Mockito
                .when(repository.findByParentId("test_id1"))
                .thenReturn(Flux.fromArray(new Category[]{category2,category2}));

        webClient.get().uri("/parentCategory/","test_id1")
                .exchange()
                .expectStatus().isOk()
                .expectBodyList(Category.class);
    }
}
